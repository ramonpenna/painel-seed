<?php

class Login_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run()
    {
        $sth = $this->db->prepare("SELECT * FROM usuarios WHERE login = :login AND senha = :senha");
        $sth->execute(array(
            ':login' => $_POST['login'],
            ':senha' => Hash::create('sha256', $_POST['senha'], HASH_PASSWORD_KEY)
        ));
        
        $data = $sth->fetch();
        
        $count =  $sth->rowCount();
        if ($count > 0) {
            // login
            Session::init();
            Session::set('logado', true);
            $_SESSION['usuario'] = $data;
            header('location: ../dashboard');
        } else {
            header('location: ../login');
        }
        
    }
    
}