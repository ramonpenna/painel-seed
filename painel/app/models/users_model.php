<?php

class Users_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function userList()
    {
        return $this->db->select("
            SELECT 
                u.id_usuario, 
                u.nome, 
                u.email, 
                u.status, 
                DATE_FORMAT(u.data_cadastro, '%d/%m/%Y') AS data_cadastro,
                n.nome AS nivel
            FROM 
                usuarios u
                INNER JOIN niveis n ON n.id_nivel = u.id_nivel
            ORDER BY
                u.data_cadastro DESC"
        );
    }

    public function nivelList()
    {
        return $this->db->select("SELECT id_nivel, nome FROM  niveis");
    }
    
    public function userSingleList($id_usuario)
    {
        return $this->db->selectSingle('SELECT * FROM usuarios WHERE id_usuario = :id_usuario', array(':id_usuario' => $id_usuario));
    }
    
    public function create($data)
    {
        
        $this->db->insert('usuarios', array(
            'nome' => $data['nome'],
            'email' => $data['email'],
            'telefone' => $data['telefone'],
            'login' => $data['login'],
            'status' => $data['status'],
            'id_nivel' => $data['nivel'],
            'senha' => Hash::create('sha256', $data['senha'], HASH_PASSWORD_KEY)
        ));
        
        $id_usuario = $this->db->lastInsertId();
        
        if ( isset( $_FILES['foto']['name'] ) && $_FILES['foto']['error'] == 0 ) {
            
            if (!file_exists('assets/images/users/' . $id_usuario)) {
                mkdir('assets/images/users/' . $id_usuario, 0755, true);
            }else{
                $files = glob('assets/images/users/' . $id_usuario . '/*'); // get all file names
                    foreach($files as $file){ // iterate files
                        if(is_file($file))
                            unlink($file); // delete file
                    }
            }
         
            $foto_tmp = $_FILES['foto']['tmp_name'];
            $nome = $_FILES['foto']['name'];
         
            $extensao = pathinfo ( $nome, PATHINFO_EXTENSION );
            $extensao = strtolower ( $extensao );
         
            if ( strstr ( '.jpg;.jpeg;.gif;.png', $extensao ) ) {
                
                $novoNome = uniqid ( time () ) . '.' . $extensao;
                $destino = 'assets/images/users/' . $id_usuario . '/' . $novoNome;
         
                if ( @move_uploaded_file ( $foto_tmp, $destino ) ) {
                    $this->db->update('usuarios', array('foto' => $novoNome), "id_usuario = {$id_usuario}");
                }else{
                    $_SESSION['notification']['msg'] = 'Erro ao salvar a foto.';
                    $_SESSION['notification']['tipo'] = "error";
                }
            }
            else{
                $_SESSION['notification']['msg'] = 'Você poderá enviar apenas fotos "*.jpg;*.jpeg;*.gif;*.png"<br />';
                $_SESSION['notification']['tipo'] = "error";
            }
        }
    }
    
    public function editSave($data)
    {
        $postData = array(
            'data_atualizacao' => date('Y-m-d H:i:s'),
            'nome' => $data['nome'],
            'email' => $data['email'],
            'telefone' => $data['telefone'],
            'login' => $data['login'],
            'status' => $data['status'],
            'id_nivel' => $data['nivel']
        );

        if (isset($data['senha']) && isset($data['confirmar-senha'])) {
            if ( !empty($data['senha']) && ($data['senha'] == $data['confirmar-senha']) ) {
                $postData['senha'] = Hash::create('sha256', $data['senha'], HASH_PASSWORD_KEY);
            }
        }
        
        if ( isset( $_FILES['foto']['name'] ) && $_FILES['foto']['error'] == 0 ) {
            
            if (!file_exists('assets/images/users/' . $data['id_usuario'])) {
                mkdir('assets/images/users/' . $data['id_usuario'], 0755, true);
            }else{
                $files = glob('assets/images/users/' . $data['id_usuario'] . '/*'); // get all file names
                    foreach($files as $file){ // iterate files
                        if(is_file($file))
                            unlink($file); // delete file
                    }
            }
         
            $foto_tmp = $_FILES['foto']['tmp_name'];
            $nome = $_FILES['foto']['name'];
         
            $extensao = pathinfo ( $nome, PATHINFO_EXTENSION );
            $extensao = strtolower ( $extensao );
         
            if ( strstr ( '.jpg;.jpeg;.gif;.png', $extensao ) ) {
                
                $novoNome = uniqid ( time () ) . '.' . $extensao;
                $destino = 'assets/images/users/' . $data['id_usuario'] . '/' . $novoNome;
         
                if ( @move_uploaded_file ( $foto_tmp, $destino ) ) {
                    $postData['foto'] = $novoNome;
                }else{
                    $_SESSION['notification']['msg'] = 'Erro ao salvar a foto.';
                    $_SESSION['notification']['tipo'] = "error";
                }
            }
            else{
                $_SESSION['notification']['msg'] = 'Você poderá enviar apenas fotos "*.jpg;*.jpeg;*.gif;*.png"<br />';
                $_SESSION['notification']['tipo'] = "error";
            }
        }
            
        $this->db->update('usuarios', $postData, "id_usuario = {$data['id_usuario']}");
        
        if($_SESSION['usuario']['id_usuario'] == $data['id_usuario']){
            $_SESSION['usuario'] = $this->db->selectSingle('SELECT * FROM usuarios WHERE id_usuario = :id_usuario', array(':id_usuario' => $data['id_usuario']));
        }

    }
    
    public function delete($id_usuario)
    {
        $result = $this->db->select('SELECT id_usuario FROM usuarios WHERE id_usuario = :id_usuario', array(':id_usuario' => $id_usuario));

        if ($result[0]['id_usuario'] == '1')
        return false;
        
        $this->db->delete('usuarios', "id_usuario = '$id_usuario'");
    }
}