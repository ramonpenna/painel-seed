<!-- START PAGE SIDEBAR -->
<div class="page-sidebar">
    <!-- START X-NAVIGATION -->
    <ul class="x-navigation">
        <li class="xn-logo">
            <a href="index.html"><?php echo CONFIG_DEFAULT_NAME; ?></a>
            <a href="#" class="x-navigation-control"></a>
        </li>
        <li class="xn-profile">
            <a href="#" class="profile-mini">
                <img src="<?php echo CONFIG_PATH_ASSETS; ?>/images/users/<?php echo $_SESSION['usuario']['id_usuario']; ?>/<?php echo $_SESSION['usuario']['foto']; ?>" alt="<?php echo $_SESSION['usuario']['nome']; ?>"/>
            </a>
            <div class="profile">
                <div class="profile-image">
                    <img src="<?php echo CONFIG_PATH_ASSETS; ?>/images/users/<?php echo $_SESSION['usuario']['id_usuario']; ?>/<?php echo $_SESSION['usuario']['foto']; ?>" alt="<?php echo $_SESSION['usuario']['nome']; ?>"/>
                </div>
                <div class="profile-data">
                    <div class="profile-data-name"><?php echo $_SESSION['usuario']['nome']; ?></div>
                </div>
                <div class="profile-controls">
                    <a href="pages-profile.html" class="profile-control-left"><span class="fa fa-info"></span></a>
                    <a href="pages-messages.html" class="profile-control-right"><span class="fa fa-envelope"></span></a>
                </div>
            </div>                                                                        
        </li>
        <li class="xn-title">Sistema</li>
        <li class="<?php echo ($this->getURL(0) == 'dashboard' ? 'active' : ''); ?>">
            <a href="<?php echo CONFIG_PATH; ?>"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a>                        
        </li>   
        <li class="xn-openable <?php echo ($this->getURL(0) == 'users' ? 'active' : ''); ?> ">
            <a href="#"><span class="fa fa-users"></span> <span class="xn-text">Usuários</span></a> 
            <ul class="animated zoomIn">
                <li class="<?php echo ($this->getURL(0) == 'users' && $this->getURL(1) == '' ? 'active' : ''); ?>">
                    <a href="<?php echo CONFIG_PATH; ?>/users">
                        <span class="fa fa-list-ul"></span> 
                        Listar
                    </a>
                </li>
                <li class="<?php echo ($this->getURL(0) == 'users' && $this->getURL(1) == 'create' ? 'active' : ''); ?>">
                    <a href="<?php echo CONFIG_PATH; ?>/users/create">
                        <span class="fa fa-plus"></span> 
                        Cadastrar
                    </a>
                </li>
            </ul>                       
        </li>              
        <li class="xn-title">Site</li>
        
    </ul>
    <!-- END X-NAVIGATION -->
</div>
<!-- END PAGE SIDEBAR -->