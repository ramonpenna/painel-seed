<!DOCTYPE html>
<html>

<head>
    <!-- META SECTION -->
    <title>Erro 404</title>            
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <!-- END META SECTION -->
    
    <style type="text/css">
        :root {
            --primary-color: <?php echo CONFIG_DEFAULT_PRIMARY_COLOR; ?>;
            --secondary-color: <?php echo CONFIG_DEFAULT_SECONDARY_COLOR; ?>;
        }
    </style>

    <link rel="stylesheet" type="text/css" id="theme" href="<?php echo CONFIG_PATH_PUBLIC; ?>/css/theme-default.css"/>

</head>

<body>

    <div class="error-container">
        <div class="error-code">404</div>
        <div class="error-text">página não encontrada</div>
        <div class="error-subtext">Infelizmente a página que você procura não foi encontrada. Por favor, aguarda um momento e tente novamente ou use uma das ações abaixo.</div>
        <div class="error-actions">                                
            <div class="row">
                <div class="col-md-6">
                    <button class="btn btn-info btn-block btn-lg" onClick="document.location.href = '<?php echo CONFIG_PATH; ?>';">Voltar ao dashboard</button>
                </div>
                <div class="col-md-6">
                    <button class="btn btn-primary btn-block btn-lg" onClick="history.back();">Voltar à página anterior</button>
                </div>
            </div>                                
        </div>
    </div>   

</body>

</html>
