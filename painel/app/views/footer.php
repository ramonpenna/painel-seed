            </div>            
        <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->

        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> <strong>Sair</strong> ?</div>
                    <div class="mb-content">
                        <p>Tem certeza que deseja sair?</p>                    
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="<?php echo CONFIG_PATH_MAIN; ?>/logout" class="btn btn-success btn-lg">Sim</a>
                            <button class="btn btn-danger btn-lg mb-control-close">Não</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
        <audio id="audio-alert" src="<?php echo CONFIG_PATH_PUBLIC; ?>/audio/alert.mp3" preload="auto"></audio>
        <audio id="audio-fail" src="<?php echo CONFIG_PATH_PUBLIC; ?>/audio/fail.mp3" preload="auto"></audio>
        <!-- END PRELOADS -->                  
        
        <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <script type="text/javascript" src="<?php echo CONFIG_PATH_PUBLIC; ?>/js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo CONFIG_PATH_PUBLIC; ?>/js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?php echo CONFIG_PATH_PUBLIC; ?>/js/plugins/bootstrap/bootstrap.min.js"></script> 
        <script type="text/javascript" src="<?php echo CONFIG_PATH_PUBLIC; ?>/js/plugins/bootstrap/bootstrap-datepicker.js"></script> 
        <script type='text/javascript' src='<?php echo CONFIG_PATH_PUBLIC; ?>/js/plugins/icheck/icheck.min.js'></script>        
        <script type="text/javascript" src="<?php echo CONFIG_PATH_PUBLIC; ?>/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="<?php echo CONFIG_PATH_PUBLIC; ?>/js/plugins/scrolltotop/scrolltopcontrol.js"></script>                
        <script type="text/javascript" src="<?php echo CONFIG_PATH_PUBLIC; ?>/js/plugins/owl/owl.carousel.min.js"></script>   
        <script type="text/javascript" src="<?php echo CONFIG_PATH_PUBLIC; ?>/js/plugins/moment.min.js"></script>
        <script type="text/javascript" src="<?php echo CONFIG_PATH_PUBLIC; ?>/js/plugins/daterangepicker/daterangepicker.js"></script>    
        
        <script type="text/javascript" src="<?php echo CONFIG_PATH_PUBLIC; ?>/js/plugins/noty/jquery.noty.js"></script>    
        <script type="text/javascript" src="<?php echo CONFIG_PATH_PUBLIC; ?>/js/plugins/noty/layouts/topCenter.js"></script>    
        <script type="text/javascript" src="<?php echo CONFIG_PATH_PUBLIC; ?>/js/plugins/noty/layouts/topLeft.js"></script>    
        <script type="text/javascript" src="<?php echo CONFIG_PATH_PUBLIC; ?>/js/plugins/noty/layouts/topRight.js"></script>    
        <script type="text/javascript" src="<?php echo CONFIG_PATH_PUBLIC; ?>/js/plugins/noty/themes/default.js"></script>    
        <!-- END PLUGINS -->     

        <!-- START TEMPLATE -->
        
        <script type="text/javascript" src="<?php echo CONFIG_PATH_PUBLIC; ?>/js/settings.js"></script>
        <script type="text/javascript" src="<?php echo CONFIG_PATH_PUBLIC; ?>/js/plugins.js"></script>        
        <script type="text/javascript" src="<?php echo CONFIG_PATH_PUBLIC; ?>/js/actions.js"></script>
        <!-- END TEMPLATE -->
        
        <?php 
            if (isset($this->js)) {
                foreach ($this->js as $js)
                {
                    echo '<script src="'.CONFIG_PATH.'/'.$js.'"></script>';
                }
            }
            
            if(isset($_SESSION['notification'])){
                if(!empty($_SESSION['notification']['msg']) && !empty($_SESSION['notification']['tipo'])){
                    echo "<script>noty({text: '".$_SESSION['notification']['msg']."', layout: 'topRight', type: '".$_SESSION['notification']['tipo']."'});</script>";
                }
                Session::set('notification', '');
            }
        ?>
        <!-- END SCRIPTS -->         
    </body>
</html>