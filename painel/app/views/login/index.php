<!DOCTYPE html>
<html lang="en" class="body-full-height">
    <head>        
        <!-- META SECTION -->
        <title><?=(isset($this->title)) ? $this->title : CONFIG_DEFAULT_TITLE; ?></title>         
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->
    
        <style type="text/css">
            :root {
                --primary-color: <?php echo CONFIG_DEFAULT_PRIMARY_COLOR; ?>;
                --secondary-color: <?php echo CONFIG_DEFAULT_SECONDARY_COLOR; ?>;
            }
        </style>
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="<?php echo CONFIG_PATH_PUBLIC; ?>/css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->                                    
    </head>
    <body>
        
        <div class="login-container">
        
            <div class="login-box animated fadeInDown">
                <div class="login-logo"></div>
                <div class="login-body">
                    <div class="login-title"><strong>Bem vindo</strong>, faça seu login</div>
                    <form class="form-horizontal" role="form" action="login/run" method="post">
                    <div class="form-group">
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="Usuário" name="login"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <input type="password" class="form-control" placeholder="Senha" name="senha"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">
                            <a href="#" class="btn btn-link btn-block">Esqueceu sua senha?</a>
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-info btn-block">Entrar</button>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="login-footer">
                    <div class="pull-left">
                        &copy; 2017 <a href="http://www.pennawd.com.br">Penna WD</a>
                    </div>
                </div>
            </div>
            
        </div>
        
    </body>
</html>






