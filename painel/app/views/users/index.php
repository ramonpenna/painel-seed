<!-- START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="#">Home</a></li>
    <li class="active">Usuários</li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
    <h2><span class="fa fa-users"></span> Usuários</h2>
</div>
<!-- END PAGE TITLE -->

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

    <div class="row">
        <div class="col-md-12">

            <!-- START DEFAULT DATATABLE -->
            <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">Lista de usuários</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>                                
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table datatable">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Email</th>
                                <th>Status</th>
                                <th>Nível</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($this->userList as $usuario){ ?>
                
                                <tr>
                                    <td><?php echo $usuario['nome']; ?></td>
                                    <td><?php echo $usuario['email']; ?></td>
                                    <td class="text-center"><?php echo $this->getStatus($usuario['status']); ?></td>
                                    <td class="text-center"><?php echo $usuario['nivel']; ?></td>
                                    <td class="text-center">
                                        <div class="visible-xs">
                                            <div class="dropdown">
                                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
                                                    <span class="fa fa-ellipsis-h"></span>
                                                    <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="./users/edit/<?php echo $usuario['id_usuario']; ?>">Editar</a></li>
                                                    <li><a href="./users/delete/<?php echo $usuario['id_usuario']; ?>">Deletar</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="hidden-xs">
                                            <a href="./users/edit/<?php echo $usuario['id_usuario']; ?>" data-toggle="tooltip" data-placement="top" title="Editar">
                                                <button class="btn btn-warning">
                                                    <span class="fa fa-pencil"></span>
                                                </button>
                                            </a>
                                            <a href="./users/delete/<?php echo $usuario['id_usuario']; ?>" data-toggle="tooltip" data-placement="top" title="Excluir">
                                                <button class="btn btn-danger">
                                                    <span class="fa fa-trash-o"></span>
                                                </button>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                
                            <?php } ?>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
            <!-- END DEFAULT DATATABLE -->

        </div>
    </div>                                
    
</div>
<!-- PAGE CONTENT WRAPPER -->                