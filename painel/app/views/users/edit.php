<!-- START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="#">Home</a></li>
    <li><a href="#">Usuários</a></li>
    <li class="active">Editar</li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">

    <div class="row">
        <div class="col-md-12">
            
            <form action="<?php echo CONFIG_PATH; ?>/users/editSave/<?php echo $this->usuario['id_usuario'] ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                
                <input type="hidden" name="id_usuario" value="<?php echo $this->usuario['id_usuario'] ?>"/>
                                            
                <div class="panel panel-default tabs">                            
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#tab-dados" role="tab" data-toggle="tab">Dados</a></li>
                    </ul>
                    <div class="panel-body tab-content">
                        <div class="tab-pane active" id="tab-dados">
                            <!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dictum dolor sem, quis pharetra dui ultricies vel. Cras non pulvinar tellus, vel bibendum magna. Morbi tellus nulla, cursus non nisi sed, porttitor dignissim erat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc facilisis commodo lectus. Vivamus vel tincidunt enim, non vulputate ipsum. Ut pellentesque consectetur arcu sit amet scelerisque. Fusce commodo leo eros, ut eleifend massa congue at.</p>-->

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">* Nome</label>
                                <div class="col-md-6 col-xs-12">                                                                                                                                                        
                                    <input type="text" name="nome" class="form-control" value="<?php echo $this->usuario['nome'] ?>" required />                                                    
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">* Email</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <input type="email" name="email" class="form-control" value="<?php echo $this->usuario['email'] ?>" required />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Telefone</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <input type="tel" name="telefone" class="form-control" value="<?php echo $this->usuario['telefone'] ?>" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">* Login</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <input type="text" name="login" class="form-control" value="<?php echo $this->usuario['login'] ?>" required />
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-3 control-label">* Status</label>
                                <div class="col-md-6">                                        
                                    <select class="form-control select" name="status" required>
                                        <option value="1" <?php echo ($this->usuario['status'] == 1 ? 'selected' : ''); ?>>Ativo</option>
                                        <option value="2" <?php echo ($this->usuario['status'] == 0 ? 'selected' : ''); ?>>Inativo</option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-3 control-label">* Nível</label>
                                <div class="col-md-6">                                        
                                    <select class="form-control select" name="nivel" required>
                                        <option value="">Selecione</option>
                    					<?php foreach($this->nivelList as $nivel){ ?>
                    						<option value="<?php echo $nivel['id_nivel'] ?>" <?php echo ($nivel['id_nivel'] == $this->usuario['id_nivel'] ? 'selected' : ''); ?>><?php echo $nivel['nome'] ?></option>
                						<?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Foto</label>
                                <div class="col-md-6 col-xs-12">                                                                                                                                        
                                    <input type="file" class="fileinput btn-primary" name="foto" id="foto" title="Selecione o arquivo"/>
                                    <span class="help-block">Imagem em formato JPG, JPEG ou PNG.</span>
                                    <?php if(!empty($this->usuario['foto'])){ ?>
                                        <img src="<?php echo CONFIG_PATH_ASSETS."/images/users/".$this->usuario['id_usuario'].'/'.$this->usuario['foto']; ?>" style="width: 150px;">
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group push-up-30">
                                <h3 class="col-md-3 col-xs-12 control-label">Alterar senha</h3>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Nova Senha</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <input type="password" name="senha" class="form-control" placeholder="Digite aqui uma nova senha...">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Confirmar senha</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <input type="password" name="confirmar-senha" class="form-control" placeholder="Confirme a senha digitada acima...">
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="panel-footer">                                                                        
                        <button class="btn btn-primary pull-right">Salvar alterações <span class="fa fa-floppy-o fa-right"></span></button>
                    </div>
                </div>                                
            
            </form>
            
        </div>
    </div>                    
    
</div>
<!-- END PAGE CONTENT WRAPPER -->    
