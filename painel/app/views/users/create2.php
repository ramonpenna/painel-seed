<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Cadastrar Usuário</h2>
        <ol class="breadcrumb">
            <li>
                <a href="index.html">Dashboard</a>
            </li>
            <li>
                <a>Usuários</a>
            </li>
            <li class="active">
                <strong>Novo Usuário</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">

	<div class="row">
	    <div class="col-lg-12">
	        <div class="ibox float-e-margins">
	            <div class="ibox-title">
	                <h5>Novo Usuário</h5>
	            </div>
	            <div class="ibox-content">
	                <form action="cadastrar" method="post" class="form-horizontal">

	                    <div class="form-group">
	                    	<label class="col-sm-2 control-label">Nome</label>

	                        <div class="col-sm-10">
	                        	<input type="text" name="nome" class="form-control" required>
                        	</div>
	                    </div>
	                    <div class="hr-line-dashed"></div>

	                    <div class="form-group">
	                    	<label class="col-sm-2 control-label">Login</label>

	                        <div class="col-sm-10">
	                        	<input type="text" name="login" class="form-control" required>
                        	</div>
	                    </div>
	                    <div class="hr-line-dashed"></div>

	                    <div class="form-group">
	                    	<label class="col-sm-2 control-label">Senha</label>

	                        <div class="col-sm-10">
	                        	<input type="password" name="senha" class="form-control" required>
                        	</div>
	                    </div>
	                    <div class="hr-line-dashed"></div>

	                    <div class="form-group">
	                    	<label class="col-sm-2 control-label">Status</label>

	                        <div class="col-sm-10">
		                        <div class="radio radio-inline">
	                                <input type="radio" id="statusAtivo" name="status" value="1" required>
	                                <label for="statusAtivo"> Ativo </label>
	                            </div>
	                            <div class="radio radio-inline">
	                                <input type="radio" id="statusInativo" name="status" value="0" required>
	                                <label for="statusInativo"> Inativo </label>
	                            </div>
                        	</div>
	                    </div>
	                    <div class="hr-line-dashed"></div>

	                    <div class="form-group">
	                    	<label class="col-sm-2 control-label">Nível</label>

	                        <div class="col-sm-10">
	                        	<select name="nivel" data-placeholder="Selecione um nível..." class="chosen-select" tabindex="2" required>
                					<option value="">Selecione</option>
                					<?php foreach($this->nivelList as $nivel){ ?>
                						<option value="<?php echo $nivel['id_nivel'] ?>"><?php echo $nivel['nome'] ?></option>
            						<?php } ?>
            					</select>
                        	</div>
	                    </div>
	                    <div class="hr-line-dashed"></div>
	                    
	                    <div class="form-group">
	                        <div class="col-sm-4 col-sm-offset-2">
	                            <button class="btn btn-primary" type="submit">Salvar</button>
	                        </div>
	                    </div>
	                </form>
	            </div>
	        </div>
	    </div>
	</div>
</div>