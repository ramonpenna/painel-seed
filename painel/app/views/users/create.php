<!-- START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="#">Home</a></li>
    <li><a href="#">Usuários</a></li>
    <li class="active">Cadastrar</li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">

    <div class="row">
        <div class="col-md-12">
            
            <form action="<?php echo CONFIG_PATH; ?>/users/create" method="post" enctype="multipart/form-data" class="form-horizontal">
                                            
                <div class="panel panel-default tabs">                            
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">Dados</a></li>
                    </ul>
                    <div class="panel-body tab-content">
                        <div class="tab-pane active" id="tab-first">
                            <!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dictum dolor sem, quis pharetra dui ultricies vel. Cras non pulvinar tellus, vel bibendum magna. Morbi tellus nulla, cursus non nisi sed, porttitor dignissim erat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc facilisis commodo lectus. Vivamus vel tincidunt enim, non vulputate ipsum. Ut pellentesque consectetur arcu sit amet scelerisque. Fusce commodo leo eros, ut eleifend massa congue at.</p>-->

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">* Nome</label></label>
                                <div class="col-md-6 col-xs-12">                                                                                                                                                        
                                    <input type="text" name="nome" class="form-control" required />                                                    
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">* Email</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <input type="text" name="email" class="form-control" required />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Telefone</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <input type="tel" name="telefone" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">* Login</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <input type="text" name="login" class="form-control" required />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">* Senha</label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <input type="password" name="senha" class="form-control" required>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-3 control-label">* Status</label>
                                <div class="col-md-6">                                        
                                    <select class="form-control select" name="status" required>
                                        <option value="1">Ativo</option>
                                        <option value="2">Inativo</option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-3 control-label">* Nível</label>
                                <div class="col-md-6">                                        
                                    <select class="form-control select" name="nivel" required>
                                        <option value="">Selecione</option>
                    					<?php foreach($this->nivelList as $nivel){ ?>
                    						<option value="<?php echo $nivel['id_nivel'] ?>"><?php echo $nivel['nome'] ?></option>
                						<?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Foto</label>
                                <div class="col-md-6 col-xs-12">                                                                                                                                        
                                    <input type="file" class="fileinput btn-primary" name="foto" id="foto" title="Selecione o arquivo"/>
                                    <span class="help-block">Imagem em formato JPG, JPEG ou PNG.</span>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="panel-footer">                                                                        
                        <button class="btn btn-primary pull-right">Cadastrar <span class="fa fa-floppy-o fa-right"></span></button>
                    </div>
                </div>                                
            
            </form>
            
        </div>
    </div>                    
    
</div>
<!-- END PAGE CONTENT WRAPPER -->    
