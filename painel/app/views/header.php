<!DOCTYPE html>
<html>

<head>
    
    <!-- META SECTION -->
    <title><?=(isset($this->title)) ? $this->title : CONFIG_DEFAULT_TITLE; ?></title> 
    <meta charset="utf-8">           
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
        
    <link rel="icon" href="<?php echo CONFIG_PATH; ?>/favicon.ico" type="image/x-icon" />
    <!-- END META SECTION --> 
    
    <style type="text/css">
        :root {
            --primary-color: <?php echo CONFIG_DEFAULT_PRIMARY_COLOR; ?>;
            --secondary-color: <?php echo CONFIG_DEFAULT_SECONDARY_COLOR; ?>;
        }
    </style>
    
    <!-- CSS INCLUDE -->        
    <link rel="stylesheet" type="text/css" id="theme" href="<?php echo CONFIG_PATH_PUBLIC; ?>/css/theme-default.css"/>
    <!-- EOF CSS INCLUDE --> 

    <?php 
        if (isset($this->css)) {
            foreach ($this->css as $css)
            {
                echo '<link href="'.CONFIG_PATH.'/'.$css.'" rel="stylesheet"></link>';
            }
        }
    ?>

</head>

<body>

<?php Session::init(); ?>

    <!-- START PAGE CONTAINER -->
    <div class="page-container">
        
        <?php include "sidebar.php"; ?>
        
        <!-- PAGE CONTENT -->
        <div class="page-content">
            
            <?php include "topbar.php"; ?>                  
