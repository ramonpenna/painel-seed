<?php

/**
* Login
*/
class Login extends Controller
{
	
	function __construct()
	{
		parent::__construct();
    }
    
    function index() 
    {    
        $this->view->title = 'Login - ' . CONFIG_DEFAULT_NAME;
  
        $this->view->render('login/index');
    }
    
    function run()
    {
        $this->model->run();
    }
}