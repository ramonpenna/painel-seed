<?php 

	/**
	* Dashboard
	*/
	class Dashboard extends Controller
	{
		
		function __construct()
		{
			if(!isset($_GET['url'])){

				header('location: ' . CONFIG_PATH_MAIN);
			}

			parent::__construct();
        	Auth::handleLogin();
		}

	    function index() 
	    {    
	        $this->view->title = 'Dashboard';
	        
	        $this->view->render('header');
	        $this->view->render('dashboard/index');
	        $this->view->render('footer');
	    }
    
	    function logout()
	    {
	        Session::destroy();
	        header('location: ' . CONFIG_PATH .  '/login');
	        exit;
	    }
	}