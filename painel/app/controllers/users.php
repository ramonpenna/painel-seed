<?php
    
    /**
    * Usuarios
    */
    class Users extends Controller {

        public function __construct() {
            parent::__construct();
        	Auth::handleLogin();
        }
        
        public function index() 
        {    
            $this->view->title = 'Usuários';

            $this->view->userList = $this->model->userList();
            
            $this->view->js  = array(
                'public/js/plugins/datatables/jquery.dataTables.min.js'
            );
            $this->view->css = array(
                'public/css/plugins/dataTables/datatables.min.css'
            );
            
            $this->view->render('header');
            $this->view->render('users/index');
            $this->view->render('footer');
        }
        
        public function create() 
        {
            $this->view->title = 'Cadastrar Usuário';

            $this->view->nivelList = $this->model->nivelList();
            $this->view->js  = array(
                'public/js/plugins/bootstrap/bootstrap-file-input.js', 
                'public/js/plugins/bootstrap/bootstrap-select.js'
            );
            // $this->view->css = array('public/css/plugins/chosen/bootstrap-chosen.css', 'public/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css');

            if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST)) {
                $this->model->create($_POST);
            header('location: ' . CONFIG_PATH . '/users');
            }
            
            $this->view->render('header');
            $this->view->render('users/create');
            $this->view->render('footer');
        }
        
        public function edit($id) 
        {

            $this->view->nivelList = $this->model->nivelList();
            $this->view->js  = array(
                'public/js/plugins/bootstrap/bootstrap-file-input.js', 
                'public/js/plugins/bootstrap/bootstrap-select.js'
            );
            // $this->view->css = array(
            //     'public/css/plugins/chosen/bootstrap-chosen.css', 
            //     'public/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css'
            // );

            $this->view->usuario = $this->model->userSingleList($id);
            
            $this->view->title = 'Editar Usuário - ' . $this->view->usuario['nome'];
            
            $this->view->render('header');
            $this->view->render('users/edit');
            $this->view->render('footer');
        }
        
        public function editSave($id)
        {
            if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST)) {
                $this->model->editSave($_POST);
            }

            header('location: ' . CONFIG_PATH . '/users/edit/' . $id);
        }
        
        public function delete($id)
        {
            $this->model->delete($id);
            header('location: ' . CONFIG_PATH . '/users');
        }
    }