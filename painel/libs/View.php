<?php

/**
* View
*/
class View
{
	
	function __construct()
	{
		
	}

	public function render($name, $noInclude = false)
	{
		require 'app/views/'.$name.'.php';
	}
        
    public function getStatus($status)
    {
        if ($status == 1) {
            return '<span class="label label-success">Ativo</span>';
        }else{
            return '<span class="label label-danger">Inativo</span>';
        }
    }
        
    public function getURL($x = false)
    {
        $url = explode("/",$_GET['url']);
        if($x === false){
            return $url;
        }else{
            return $url[$x];
        }
    }
}