<?php
/**
 * 
 */
class Auth
{
    
    public static function handleLogin()
    {
        @session_start();
        $logged = $_SESSION['logado'];
        if ($logged == false) {
            session_destroy();
            header('location: ./login');
            exit;
        }
    }
    
}